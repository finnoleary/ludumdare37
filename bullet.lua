bullet = {
    bullets = {}
}

function bullet.new(self, x, y, xv, yv)
    xv, yv = magic(xv, yv)
    table.insert(bullets, inherit_funcs({
        x  = x,
        y  = y,
        xv = xv,
        yv = yv,
        size = 8,
        speed = 200,
    }, bullet))
end

function bullet.move(self, dt)
    x, y = magic(self.xv, yv)
    self.x = self.x + (self.speed*x) * dt
    self.y = self.y + (self.speed*y) * dt
end

function bullet.test_collide(self, dt, map)
    xv, yv = self.xv, self.yv
    local ts = map.tile_size
    local nx = ((self.x + (self.speed*xv) * dt) / ts)
    local ny = ((self.y + (self.speed*yv) * dt) / ts)
    nx = math.floor(nx) + 1
    ny = math.floor(ny) + 1

    if map:tile(nx, ny) == 1 then
        return true
    end
end

function bullet.update(self, dt, map, play)
    if self:test_collide(self, dt, room
