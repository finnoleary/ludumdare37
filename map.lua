
function stile(map, x, y, v)
    if (x > map.width or x < 1 or y > map.height or y < 1) then
        return
    end
    map[y][x] = v
end

function tile(map, x, y)
    if (x > map.width or x < 1 or y > map.height or y < 1) then
        return
    end
    return map[y][x]
end

function draw_map(map)
    for y = 1, map.height do
        for x = 1, map.width do
            if map:tile(x, y) == 1 then
                love.graphics.setColor(255, 255, 255)
            else
                love.graphics.setColor(24, 24, 24)
            end
            ts = map.tile_size
            love.graphics.rectangle("fill", x*ts - ts/2, y*ts - ts/2, ts, ts)
        end
    end
end

function init_map(width, height, tilesize)
    local map = {}
    for y = 1, height do
        map[y] = {}
        for x = 1, width do
            map[y][x] = 0
        end
    end
    for y = 1, height do
        map[y][1] = 1
        map[y][width] = 1
    end
    for x = 1, width do
        map[1][x] = 1
        map[height][x] = 1
    end
    map.width = width
    map.height = height
    map.tile_size = tilesize
    map.stile = stile
    map.tile = tile
    map.draw = draw_map
    return map
end
