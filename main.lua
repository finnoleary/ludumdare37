require 'util'
require 'map'
require 'player'

function love.load()
    room = init_map(49, 36, 16)
end

down = love.keyboard.isDown
function love.update(dt)
    local u = (down("left") or down("a")) and -1 or 0
    u = (down("right") or down("d")) and 1 or u
    local v = (down("up") or down("w")) and -1 or 0
    v = (down("down") or down("s")) and 1 or v

    player.sprint = down("lshift") or down("rshift")
    player.speed = player.sprint and 300 or 100
    print(player.sprint and "true" or "false", player.speed)

    if not player:test_collide(u, v, dt, room) then
        player:move(u, v, dt)
    end
end

function love.draw()
    room:draw()
    player:draw()
end
