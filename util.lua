function magic(xv, yv)
    -- like normalize(vector(xv, yv))
    -- from http://higherorderfun.com/blog/2012/06/03/math-for-game-programmers-05-vector-cheat-sheet/
    local l = math.sqrt(xv*xv + yv*yv)
    if l > 0 then
        return xv/l, yv/l
    else
        return xv, yv
    end
end

function inherit_funcs(thing, table)
    for n, v in pairs(table) do
        if type(v) == "function" then
            thing[n] = v
        end
    end
    return thing
end

function rectpt(a, bx, by)
    local x, y, w, h = a[0], a[1], a[2], a[3]
    return bx >= x and bx < x+w and by >= y and by < y+h
end
