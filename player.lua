player = {
    x = love.graphics.getWidth()/2,
    y = love.graphics.getHeight()/2,
    speed = 100,
    sprint = false,
}

function player.move(self, x, y, dt)
    x, y = magic(x, y)
    player.x = player.x + (player.speed*x) * dt
    player.y = player.y + (player.speed*y) * dt
end

function player.test_collide(self, xv, yv, dt, map)
    local ts = map.tile_size
    local nx = ((player.x + (player.speed*xv) * dt) / ts)
    local ny = ((player.y + (player.speed*yv) * dt) / ts)
    nx = math.floor(nx) + 1
    ny = math.floor(ny) + 1

    if map:tile(nx, ny) == 1 then
        return true
    end
end

function player.draw(self)
    love.graphics.setColor(255, 0, 0)
    love.graphics.rectangle("fill", self.x, self.y, 16, 16)
end
